FROM python:latest

ADD  . /app 

RUN pip install Flask
CMD ["python","app.py"]
